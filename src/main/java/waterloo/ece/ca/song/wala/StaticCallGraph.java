package waterloo.ece.ca.song.wala;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.ibm.wala.classLoader.CallSiteReference;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.AnalysisCacheImpl;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilder;
import com.ibm.wala.ipa.callgraph.CallGraphBuilderCancelException;
import com.ibm.wala.ipa.callgraph.CallGraphStats;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.AnalysisOptions.ReflectionOptions;
import com.ibm.wala.ipa.callgraph.impl.DefaultEntrypoint;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.ipa.cha.ClassHierarchyFactory;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.config.AnalysisScopeReader;
import com.ibm.wala.util.graph.traverse.DFS;
import com.ibm.wala.util.io.FileProvider;
import com.ibm.wala.util.strings.StringStuff;

public class StaticCallGraph {
	/*
	 * If given -mainClass, uses main() method of class_name as entrypoint. 
	 * If given -entryClass, uses all public methods of class_name as entrypoint.
	 */
	private String mainClass; 
	private String entryClass;
	private String scopefile;
	private String exclusionfile;
	private CallGraph cg;
	private AnalysisScope scope;
	
	public String getEntryClass() {
		return entryClass;
	}

	public void setEntryClass(String entryClass) {
		this.entryClass = entryClass;
	}

	public String getMainClass() {
		return mainClass;
	}

	public void setMainClass(String mainClass) {
		this.mainClass = mainClass;
	}

	public String getScopefile() {
		return scopefile;
	}

	public void setScopefile(String scopefile) {
		this.scopefile = scopefile;
	}

	public String getExclusionfile() {
		return exclusionfile;
	}

	public void setExclusionfile(String exclusionfile) {
		this.exclusionfile = exclusionfile;
	}

	public CallGraph getCg() {
		return cg;
	}

	public boolean generateCallGraph()throws IOException, ClassHierarchyException, 
	IllegalArgumentException, CallGraphBuilderCancelException{
		
		if(this.exclusionfile == null || this.scopefile == null)
			return false;
		if(this.mainClass == null && this.entryClass == null)
			return false;
		
		File exFile = new FileProvider()
				.getFile(this.exclusionfile);
		this.scope = AnalysisScopeReader.makeJavaBinaryAnalysisScope(this.scopefile,exFile);
		
		 IClassHierarchy cha = ClassHierarchyFactory.make(scope);
		   AnalysisOptions options = new AnalysisOptions();
			Iterable<Entrypoint> entrypoints = entryClass != null ? makePublicEntrypoints(
					scope, cha, entryClass) : Util.makeMainEntrypoints(scope, cha, this.mainClass);
			options.setEntrypoints(entrypoints);

			options.setReflectionOptions(ReflectionOptions.NONE);
			
			AnalysisCache cache = new AnalysisCacheImpl();
			// other builders can be constructed with different Util methods
			CallGraphBuilder builder = Util.makeZeroOneContainerCFABuilder(options,
					cache, cha, scope);
			
			//CallGraphBuilder builder = 
			
			// Rapid Type Analysis
			//com.ibm.wala.ipa.callgraph.impl.Util.makeRTABuilder(options, new AnalysisCache(), cha, scope); 
			
			// 0-1-CFA = context-insensitive, allocation-site-based heap
			//com.ibm.wala.ipa.callgraph.impl.Util.makeZeroOneCFABuilder(options, new AnalysisCache(), cha, scope); 
			
			// 0-1-Container-CFA = object-sensitive container
			//com.ibm.wala.ipa.callgraph.impl.Util.makeZeroOneContainerCFABuilder(options, new AnalysisCache(), cha, scope); 

			System.out.println("building call graph...");
			long start = System.currentTimeMillis();
			this.cg = builder.makeCallGraph(options, null);
			long end = System.currentTimeMillis();
			
			if(this.cg != null){
		    System.out.println("done");
		    System.out.println("took " + (end-start) + "ms");
		    //System.out.println(CallGraphStats.getStats(cg));
		    //System.out.println(cg.toString());
		    
		    return true;
		   }
			else{
				System.out.println("failing...");
				return false;
			}
	}
	
	// for collecting call graph entrypoints
	  private static Iterable<Entrypoint> makePublicEntrypoints(AnalysisScope scope, IClassHierarchy cha, String entryClass) {
		    Collection<Entrypoint> result = new ArrayList<Entrypoint>();
		    IClass klass = cha.lookupClass(TypeReference.findOrCreate(ClassLoaderReference.Application,
		        StringStuff.deployment2CanonicalTypeString(entryClass)));
		   if(klass == null)
			   return result;
		    for (IMethod m : klass.getDeclaredMethods()) {
		      if (m.isPublic()) {
		        result.add(new DefaultEntrypoint(m, cha));
		      }
		    }
		    return result;
		  }
	
	public ArrayList<String> collectMethodCoverage(String ClassName, String methodName, String methodDescriptor){
		//TypeReference t = TypeReference.findOrCreate(ClassLoaderReference.Application, "Lorg/apache/camel/builder/xml/DefaultNamespaceContext");
		//MethodReference m = MethodReference.findOrCreate(t, "setNamespacesFromDom", "(Lorg/w3c/dom/Element;)V");
		ArrayList<String> coverages = new ArrayList<String>();
		TypeReference t = TypeReference.findOrCreate(ClassLoaderReference.Application, ClassName);
		MethodReference m = MethodReference.findOrCreate(t, methodName, methodDescriptor);
		        
	    Set<CGNode> node = this.cg.getNodes(m);
		    
		    for(CGNode n: node){
		    	for(Iterator<CallSiteReference> i = n.iterateCallSites();i.hasNext();){
		    		CallSiteReference site = i.next();
		    		//System.out.println(site.toString());
		    			coverages.add(site.toString());
		    	}
		   }
		    
		 return coverages;   
	}
	
	/**
	 * get all the method and its descriptor of a class
	 *  method <-> description 
	 */
	public Map <String,String> methodAndDescriptor(String className, String prefix) throws ClassHierarchyException{
		 //Lorg/apache/camel/builder/xml/DefaultNamespaceContext
		 Map<String, String> methodDes = new HashMap<String, String>();
		 
		 IClassHierarchy cha = ClassHierarchyFactory.make(scope);
		 for (IClass c : cha) {
			if (!scope.isApplicationLoader(c.getClassLoader()))
				continue;
			String cname = c.getName().toString();
			
			if(cname.contains(className)){
				System.out.println("Class: " + cname);
				for (IMethod m: c.getAllMethods()) {
						if(m.getName().toString().toLowerCase().contains("test") && m.toString().contains(prefix)){
							System.out.println(m.toString());
							methodDes.put(m.getName().toString(), m.getDescriptor().toString());
						}
				}
			}
		} 
		 
	   return methodDes;
	}
	
	public void write2file(Map<String, ArrayList<String>> method_coverage, String output) throws IOException{
		FileWriter	writer = new FileWriter(output, false);
		
		for(Map.Entry<String, ArrayList<String>> entry: method_coverage.entrySet()){
			String testcase = entry.getKey();
			ArrayList<String> involved = entry.getValue();
			writer.write(testcase);
			writer.flush();
			writer.write(System.getProperty("line.separator"));
			writer.write("[");
			writer.flush();
			
			for(String str: involved){
				 writer.write(String.valueOf(str)+"	");
				 writer.flush();
			}
			writer.write("]");
			writer.write(System.getProperty("line.separator"));
			writer.flush();
		}
		writer.close();
	}
	
	/**
	 * get all the test class
	 * Lorg/apache/camel/builder/xml/DefaultNamespaceContext
	 *
	 */
	public ArrayList<String> getAllTestClass() throws ClassHierarchyException{
		 ArrayList<String> testClasses = new ArrayList<String>();
		 
		 IClassHierarchy cha = ClassHierarchyFactory.make(scope);
		 for (IClass c : cha) {
			if (!scope.isApplicationLoader(c.getClassLoader()))
				continue;
			String cname = c.getName().toString();
			
			if(cname.toLowerCase().contains("test")){
				testClasses.add(cname);
			}
		} 
		 
	   return testClasses;
	}
	
	
	public static void main(String args[]) throws IOException, ClassHierarchyException, 
	IllegalArgumentException, CallGraphBuilderCancelException {
		
		StaticCallGraph scg = new StaticCallGraph();
		
		String entryClass = "org/apache/camel/builder/xml/XPathWithNamespacesFromDomTest";
		String mainClass = "L"+entryClass; //"Lorg/apache/camel/builder/xml/XPathWithNamespacesFromDomTest";
		String exclufile = System.getProperty("user.dir")+"/data/exclusion.txt";
		String scopefile = System.getProperty("user.dir")+"/data/camel1.2.jar";
		String prefix = "org/apache/camel"; //filter jdk functions
		String output = System.getProperty("user.dir")+"/data/test_coverage.txt";
		
		scg.setExclusionfile(exclufile);
		scg.setEntryClass(entryClass);
		scg.setScopefile(scopefile);
		
		scg.generateCallGraph();
		//System.out.println(scg.getCg());
		
		//get all the test method
		Map<String, String> test_methods = new HashMap<String, String>();
		test_methods = scg.methodAndDescriptor(mainClass, prefix);
		
		Map<String, ArrayList<String>> method_coverage = new HashMap<String, ArrayList<String>>();
		
		for(Map.Entry<String, String> entry: test_methods.entrySet()){
			String methodName = entry.getKey();
			String methodDescriptor = entry.getValue();
			ArrayList<String> coverage = new ArrayList<String>();
			coverage = scg.collectMethodCoverage(mainClass, methodName, methodDescriptor);
			
			method_coverage.put(methodName, coverage);
		}
		
		for(Map.Entry<String, ArrayList<String>> entry: method_coverage.entrySet()){
			String testcase = entry.getKey();
			ArrayList<String> involved = entry.getValue();
			System.out.println("testcase: "+testcase);
			for(String str: involved){
				System.out.println(str);
			}
			System.out.println("===");
		}
		
		scg.write2file(method_coverage, output);	  
	}
} 